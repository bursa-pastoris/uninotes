`uninotes` is a cleaned-up and reworked version of a LaTeX class I used in the
past to write university lceture notes.  Today I maintain it for some of them
that I distribute.

It's based on `scrbook`, `classicthesis` and `arsclassica` and does not do
anything special: it just loads and configures packages, plus some
customization.  I use it just to avoid rewriting the same tens of rows whenever
I started a new note book.


## Features

`uninotes` provides the following features.  Most of them are just package
loadings.

- some units of measurement that `siunitx` doesn't provide (cal, €, l, q)
- custom layout with `scrbook`, `classicthesis` and `arsclassica`
- encoding with `inputenc` (UTF8) and `fontenc` (T1)
- footnotes with custom symbols[^fn_symbols]
- tables with `booktabs`, `tabularx` and `siunitx` (for `S` columns)
- figures with `graphicx` and `subfig`
- captions with `captions`: captions label are in bold and sans font. The
  caption must be placed above tables but below figures.
- for math, for the Greek letters with more than one version (e.g. `\pi` and
  `\varpi`) the following are used: `\varepsilon`, `\vartheta`, `\pi`, `\rho`,
  `\sigma` and `\varphi`
- chemical formulas with `chemformula`
- references and hyperreferences with `hyperref`, `varioref` and `cleveref`.
  - `uninotes` does not use abbreviations (e.g. *eq.* for *equation*)
  - for the Italian language, `section` level titles are referred to as
    *paragrafo* instead of *sezione*
- remove metadata with `pdfprivacy`: title, subject, author and keywords are
  allowed, any other data is omitted (including PDF trailer ID)
- new commands:
  - `\email{arg}`: writes an email address with a `mailto:` `href`
  - `\sn{arg}`: writes scientific names with emphasis
  - `\en{arg}`: writes English text with emphasis (for languages other than
    English)

The document language must be passed as an argument to `uninotes`.  If more
than one language is used they must all be passed, the main one being the last
passed.

Of course, any other needed package can be passed in the preamble.

## License

`uninotes` is released under the [Expat license](./LICENSE).



[^fn_symbols]: The symbols are `*`, `†`, `‡` and `§` in this order, then the
    same doubled, then the same tripled.
