% Copyright 2022, 2023 bursa-pastoris
%
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
% ---
%
% This class' repository is available at https://git.disroot.org/bursa-pastoris/uninotes.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{uninotes}[2023/03/01 University notes class]
\LoadClass{scrbook}
\KOMAoptions{headinclude=true,footinclude=true}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

% Language
\RequirePackage{babel}

% Appearance
\RequirePackage{classicthesis}
\RequirePackage{arsclassica}
\RequirePackage[symbol,bottom]{footmisc}
\DefineFNsymbols{uninotesfn}{{*}{$\dagger$}{$\ddagger$}{$\S$}%
    {**}{$\dagger\dagger$}{$\ddagger\ddagger$}{$\S\S$}%
    {***}{$\dagger\dagger\dagger$}{$\ddagger\ddagger\ddagger$}{$\S\S\S$}}
\setfnsymbol{uninotesfn}

% Tables, numbers, units of measurement
\RequirePackage{booktabs}
\RequirePackage{tabularx}

\RequirePackage{eurosym}

\RequirePackage{siunitx}
\sisetup{output-decimal-marker={,},
    range-phrase={-},
    range-units=single,
    uncertainty-mode=separate}
\DeclareSIUnit{\cal}{cal}
\DeclareSIUnit{\eur}{\geneuro{}}
\DeclareSIUnit{\litre}{l}
\DeclareSIUnit{\quintal}{q}

% Figures
\RequirePackage{graphicx}
\RequirePackage{subfig}
\RequirePackage[inkscapelatex=no,
        inkscapepath={.svg_export}]{svg}

% Floats
\RequirePackage{caption}
\captionsetup{tableposition=top,%
        figureposition=bottom,%
        labelfont={sf,bf}}

% Index
\usepackage{imakeidx}
\makeindex[intoc,columns=2]

% Table of contents
\setcounter{tocdepth}{2}

% Math
\renewcommand{\epsilon}{\varepsilon}
\renewcommand{\theta}{\vartheta}
\renewcommand{\varpi}{\pi}
\renewcommand{\varrho}{\rho}
\renewcommand{\varsigma}{\sigma}
\renewcommand{\phi}{\varphi}

% Chemistry
\RequirePackage{chemformula}

% Bibliography
\usepackage[autostyle,italian=guillemets]{csquotes}
\usepackage[bibstyle=numeric,%
        citestyle=numeric-comp,%
        babel=hyphen,%
        backend=biber]%
    {biblatex}

% (Hyper)References
\RequirePackage{hyperref}
\RequirePackage{varioref}
\RequirePackage[noabbrev]{cleveref}
\creflabelformat{equation}{#2#1#3}
\AddBabelHook[italian]{translate}{patterns}%
    {\crefname{section}{paragrafo}{paragrafi}%
     \Crefname{section}{Paragrafo}{Paragrafi}}
\RequirePackage[nodocdata=false,
        noeditdata=true,
        noproducerdata=true,
        noptexdata=true,
        nopdftrailerid=true]%
    {pdfprivacy}

% Other features
\DeclareRobustCommand{\mail}[1]{\href{mailto:#1}{\texttt{#1}}}
\providecommand{\sn}[1]{\emph{#1}} % Scientific names
\providecommand{\en}[1]{\emph{\foreignlanguage{english}{#1}}}
